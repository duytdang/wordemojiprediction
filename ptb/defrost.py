import tensorflow as tf
import argparse
import numpy as np
import json

def load_graph():
    # We load the protobuf file from the disk and parse it to retrieve the 
    # unserialized graph_def

    frozen_graph_filename = "C:/Users/Dell/Documents/New_folder/tensorflow/models-master/tutorials/rnn/ptb/frozen/frozen_model_Hypertest.pb"

    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we can use again a convenient built-in function to import a graph_def into the 
    # current default Graph
    with tf.Graph().as_default() as graph:
        tf.import_graph_def(
            graph_def, 
            input_map=None, 
            return_elements=None, 
            name="prefix",
            op_dict=None, 
            producer_op_list=None
        )
    return graph

if __name__ == '__main__':

    # We use our "load_graph" function
    graph = load_graph()

    # We can verify that we can access the list of operations in the graph
    for op in graph.get_operations():
        print(op.name)
        # prefix/Placeholder/inputs_placeholder
        # ...
        # prefix/Accuracy/predictions
        
    # We access the input and output nodes
    logits = graph.get_tensor_by_name('prefix/Hypertest/Model/logits:0')
    inputs_placeholder = graph.get_tensor_by_name('prefix/Hypertest/Placeholder/inputs_placeholder:0')
        
    # # We launch a Session
    # with tf.Session(graph=graph) as sess:
    #     # Note: we didn't initialize/restore anything, everything is stored in the graph_def
    #     y_out = sess.run(y, feed_dict={
    #         x: [[3, 5, 7, 4, 5, 1, 1, 1, 1, 1]] # < 45
    #     })
    #     print(y_out) # [[ False ]] Yay, it works!


    with tf.Session(graph=graph) as sess:
        # Note: we didn't initialize/restore anything, everything is stored in the graph_def

        with open('dictionary.json') as inputFile:
            dictionary = json.load(inputFile)
        inverseDictionary = dict(zip(dictionary.values(), dictionary.keys()))

        word1 = "the"
        word2 = "dow"

        feed_dict = {}
        # feed_dict[inputs_placeholder] = [[dictionary[word1],dictionary[word2]],[dictionary["<unk>"],dictionary["<unk>"]]]

        feed_dict[inputs_placeholder] = [[321,214],[413,1234]]

        predictions = sess.run(logits, feed_dict)

        print(predictions[0][1][0])
        print(predictions[0][1][1])
        print(predictions[0][1][2])
        print(predictions[0][1][3])
        print(predictions[0][1][4])
        print(predictions[0][1][5])
        print(predictions[0][1][6])
        print(predictions[0][1][7])
        print(predictions[0][1][8])
        print(predictions[0][1][9])
        print(predictions[0][1][10])
        print(predictions[0][1][11])
        print(predictions[0][1][12])
        print(predictions[0][1][13])
        print(predictions[0][1][14])
        print(predictions[0][1][15])
        print(predictions[0][1][16])
        print(predictions[0][1][17])
        print(predictions[0][1][18])
        print(predictions[0][1][19])
        print(predictions[0][1][20])

        
        decodedWordId1 = int(np.argmax(predictions[0][0]))
        decodedWordId2 = int(np.argmax(predictions[0][1]))

        print("input1:" + word1)
        print("input2:" + word2)

        print ("prediction1:" + inverseDictionary[decodedWordId1])
        print ("prediction2:" + inverseDictionary[decodedWordId2])
        print ("------")
        print ("      ")